Laboratory 3 – Control of a Gripen Aircraft

I have not finalize this lab yet.

# Included Contents
### Live Scripts:
- [main_gripen.mlx](main.mlx)

### Simulink Models:
- [gripen_model.slx](gripen_model.slx)
![alt text](figures/gripen_model.jpeg)

- [simulation_enl_tracking.slx](simulation_enl_tracking.slx)
![alt text](figures/simulation_enl_tracking.jpeg)

- [simulation_regulated.slx](simulation_regulated.slx)
![alt text](figures/simulation_regulated.jpeg)

